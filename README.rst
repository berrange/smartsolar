Smart Solar
===========

This application provides a command line tool for analysing the potential for solar
generation at a location, in comparison with real world electricity consumption data
obtained from a smart meter, and potential on site battery storage.

The solar generation data is obtained from the free PVGIS service REST API

  https://joint-research-centre.ec.europa.eu/photovoltaic-geographical-information-system-pvgis_en

The smart meter electricity consumption data can be acquired from a couple of
sources

n3rgy
-----

This service is only available to UK properties with a smart meter present, and
n3rgy can only provide the most recent 3 months worth of data, with their storage
starting from when you first register your meter with them. Obtaining a complete
year's data set requires visiting the site periodically over the course of one
year.

  https://www.n3rgy.com/consumer/

It will provide a CSV file with 1 row per sample, and 30 minute granularity
on kWh consumption, matching the following format::

  "timestamp (UTC)","energyConsumption (kWh)"
  "2022-10-26 00:00","0.165"
  "2022-10-26 00:30","0.169"
  "2022-10-26 01:00","0.167"
  "2022-10-26 01:30","0.064"
  "2022-10-26 02:00","0.074"
  "2022-10-26 02:30","0.087"
  ...snip...

Octopus
-------

This service is only available to UK properties with a smart meter present, using
Octopus Energy as their supplier. From the account dashboard, choose "My Energy"
then scroll down to "Get your geek on" to download the electricity consumption in
a CSV file.

  https://octopus.energy/dashboard

It will provide a CSV file with 1 row per sample, and 30 minute granularity
on kWh consumption, matching the following format::

  Consumption (kWh), Start, End
  0.12400000000000000000, 2023-11-01T00:00:00+00:00, 2023-11-01T00:30:00+00:00
  0.07600000000000000000, 2023-11-01T00:30:00+00:00, 2023-11-01T01:00:00+00:00
  0.09400000000000000000, 2023-11-01T01:00:00+00:00, 2023-11-01T01:30:00+00:00
  0.09100000000000000000, 2023-11-01T01:30:00+00:00, 2023-11-01T02:00:00+00:00
  0.08700000000000000000, 2023-11-01T02:00:00+00:00, 2023-11-01T02:30:00+00:00
  0.08700000000000000000, 2023-11-01T02:30:00+00:00, 2023-11-01T03:00:00+00:00
  ...snip...

Other
-----

It is possible to use other data sources if the consumption information can be
transformed into a CSV file matching either of the above two formats.


Installation
------------

The tool can be installed in a virtual environment using::

  $ python -m venv /some/dir
  $ /some/dir/bin/python3 -m pip install .
  $ source /some/dir/bin/activate

and then run by invoking::

  $ smartsolar-summary

and will print out a summary of the solar generation potential and
associated consumption information::

  $ smartsolar-summary --config myhouse.yaml
  Production (Array southeast): 6814 kwh
  Production (Array northwest): 4660 kwh
  Production (Total): 11475 kwh
  Consumption: 2947 kwh
  Production (Relative): 9991 kwh
  Production (Excess): 8527 kwh
  Production (Consumed): 1291 kwh
  Grid (Import): 1655 kwh
  Grid (Export): 8699 kwh
  Grid (Flow): 7044 kwh
  Battery Production (Consumed): 2649 kwh
  Battery Grid (Import): 298 kwh
  Battery Grid (Export): 7342 kwh
  Battery Grid (Flow): 7044 kwh
  Battery (Charged): 1357 kwh

To aid in post-processing, this summary can also be output in CSV format
using the ``--csv`` argument.

Alternatively the full raw data over a year for each statistic can be written
to CSV files for loading into other applications, (gnuplot, spreadsheets,
etc)::

  $ smartsolar-dump --config myhouse.yaml
  $ ls *csv
  consumption-no-solar.csv
  grid-export-no-battery.csv
  grid-export-with-battery.csv
  grid-flow-no-battery.csv
  grid-flow-with-battery.csv
  grid-import-no-battery.csv
  grid-import-with-battery.csv
  production-array-southeast.csv
  production-consumed-no-battery.csv
  production-consumed-with-battery.csv
  production-relative.csv
  production-total.csv
  storage-charged-with-battery.csv
  storage-discharged-with-battery.csv
  storage-flow-with-battery.csv
  storage-level-with-battery.csv

The raw data over a year for each statistic can be visualized in
a local web browser, one page each, using plotly::

  $ smartsolar-display --config myhouse.yaml

Each chart can then be written out to individual HTML files::

  $ smartsolar-report --config myhouse.yaml --split

Or all charts can be written out to a single HTML file::

  $ smartsolar-report --config myhouse.yaml

To evalulate different deployment options, it is possible to produce
summary data across a range of scenarios in CSV format. This data can be
imported to a spreadsheet to graph the behavour of different scenarios. To
run scenarios tracking panel numbers, use ``--array min:step:max``:

  $ smartsolar-scenarios --config myhouse.yaml --array 2:2:20

Similarly to run scenarios tracking battery sizes, use ``--battery`` giving
data in kWh

  $ smartsolar-scenarios --config myhouse.yaml --battery 2:2:20

If a configuration has multiple arrays, then the scenario ranges must be
provided for each array. To leave a particular array unchanged, use ``::``.
For example, with two arrays in the configuration file, to run scenarios on
the size of the 2nd array use:

  $ smartsolar-scenarios --config myhouse.yaml --array :: --array 2:2:20
