#!/usr/bin/python
# SPDX-License-Identifier: GPL-2.0-or-later

from clint.textui import progress
import itertools
import sys
import yaml

from .record import *


class Report:
    def __init__(self, label):
        self.label = label
        self.productionArrays = []
        self.consumption = Consumption()

        self.productionTotal = None
        self.productionRelative = None
        self.productionConsumed = None

        self.gridImport = None
        self.gridExport = None
        self.gridFlow = None

        self.batteryProductionConsumed = None
        self.batteryGridImport = None
        self.batteryGridExport = None
        self.batteryGridFlow = None

        self.batteryStorageCharged = None
        self.batteryStorageDischarged = None
        self.batteryStorageFlow = None
        self.batteryStorageLevel = None

    @classmethod
    def load_file(cls, filename):
        with open(filename, "rb") as fh:
            cfg = yaml.safe_load(fh)
            return cls.load_config(cfg)

    @staticmethod
    def build_scenarios(cfg, array, battery):
        arraydata = []
        batterydata = []

        for idx in range(len(array)):
            entry = array[idx]
            if entry == "::":
                arraydata.append([cfg.get("solar-arrays", [])[idx]["panels"]])
            else:
                params = entry.split(":")
                if len(params) != 3:
                    print("Array scenario {entry} must have min:step:max",
                          file=sys.stderr)
                    sys.exit(1)
                data = []
                for count in range(int(params[0]), int(params[2])+1, int(params[1])):
                    data.append(count)
                arraydata.append(data)

        for idx in range(len(battery)):
            entry = battery[idx]
            if entry == "::":
                batterydata.append([cfg.get("battery", [])[idx]["capacity"]])
            else:
                params = entry.split(":")
                if len(params) != 3:
                    print("Battery scenario {entry} must have min:step:max",
                          file=sys.stderr)
                    sys.exit(1)
                data = []
                for count in range(int(params[0]), int(params[2])+1, int(params[1])):
                    data.append(count)
                batterydata.append(data)

        return arraydata, batterydata

    @classmethod
    def load_scenarios(cls, filename, array, battery):
        with open(filename, "rb") as fh:
            cfg = yaml.safe_load(fh)

            if len(array) == 0:
                for arr in cfg.get("solar-arrays", []):
                    array.append("::")
            elif len(cfg.get("solar-arrays", [])) != len(array):
                print("Config has %d arrays, but only %d scenarios provided" %
                      (len(cfg.get("solar-arrays", [])), len(array)), file=sys.stderr)
                sys.exit(1)

            if len(battery) == 0:
                for bat in cfg.get("battery", []):
                    battery.append("::")
            elif len(cfg.get("battery", [])) != len(battery):
                print("Config has %d batteries, but only %d scenarios provided" %
                      (len(cfg.get("battery", [])), len(array)), file=sys.stderr)
                sys.exit(1)

            arraydata, batterydata = cls.build_scenarios(cfg, array, battery)

            allarrays = list(itertools.product(*arraydata))
            allbatteries = list(itertools.product(*batterydata))
            scenarios = []

            tot = len(allarrays) * len(allbatteries)
            val = 0
            with progress.Bar(expected_size=tot,
                              label="Load scenarios: ") as bar:
                for arrayentry in allarrays:
                    for batteryentry in allbatteries:
                        bar.show(val)
                        val = val + 1
                        for idx in range(len(arrayentry)):
                            cfg.get("solar-arrays", [])[idx]["panels"] = arrayentry[idx]
                        for idx in range(len(batteryentry)):
                            cfg.get("battery", [])[idx]["capacity"] = batteryentry[idx]
                        scenarios.append(Report.load_config(cfg))

            return scenarios

    @classmethod
    def load_config(cls, cfg):
        location = Location(**cfg.get("location", {}))
        database = Database(**cfg.get("database", {}))
        batteries = cfg.get("battery", [])

        label = []
        for arraycfg in cfg.get("solar-arrays", []):
            label.append("pv:%d" % arraycfg["panels"])
        for bat in batteries:
            label.append("bat:%d" % bat["capacity"])

        self = cls(label=";".join(label))

        # Maximum battery capacity
        batmax = 0
        for bat in batteries:
            batmax += bat.get("capacity", 0)

        for arraycfg in cfg.get("solar-arrays", []):
            array = SolarArray(**arraycfg)

            array.fetch_pvgis(location, database)
            self.productionArrays.append(array)

        for entry in cfg.get("consumption", []):
            if entry.get("format") == "n3rgy":
                self.consumption.load_n3rgy(entry.get("data"))
            elif entry.get("format") == "octopus":
                self.consumption.load_octopus(entry.get("data"))
                                            
            
        self.productionTotal = ProductionTotal(self.productionArrays)
        self.productionRelative = ProductionRelative(
            self.productionTotal, self.consumption
        )
        self.productionConsumed = ProductionConsumed(
            self.productionRelative, self.consumption
        )
        self.gridImport = GridImport(self.productionRelative, self.consumption)
        self.gridExport = GridExport(self.productionRelative, self.consumption)
        self.gridFlow = GridFlow(self.gridImport, self.gridExport)

        if batmax == 0:
            return

        self.batteryProductionConsumed = BatteryProductionConsumed()
        self.batteryGridImport = BatteryGridImport()
        self.batteryGridExport = BatteryGridExport()
        self.batteryGridFlow = BatteryGridFlow()

        self.batteryCharged = BatteryStorageCharged()
        self.batteryDischarged = BatteryStorageDischarged()
        self.batteryFlow = BatteryStorageFlow()
        self.batteryLevel = BatteryStorageLevel()

        # Running battery level
        bat = 0

        for consslice in self.consumption.slices.values():
            prodslice = self.productionRelative.find(consslice.key())

            if consslice.power_kwh > prodslice.power_kwh:
                shortfall = consslice.power_kwh - prodslice.power_kwh

                discharge = shortfall
                if discharge > bat:
                    discharge = bat

                bat -= discharge
                shortfall -= discharge

                self.batteryProductionConsumed.add_slice(prodslice.delta(discharge))
                self.batteryGridExport.add_slice(prodslice.derive(0))
                self.batteryGridImport.add_slice(prodslice.derive(shortfall))
                self.batteryGridFlow.add_slice(prodslice.derive(shortfall * -1))
                self.batteryCharged.add_slice(prodslice.derive(0))
                self.batteryDischarged.add_slice(prodslice.derive(discharge))
                self.batteryFlow.add_slice(prodslice.derive(discharge * -1))
                self.batteryLevel.add_slice(prodslice.derive(bat))

            else:
                excess = prodslice.power_kwh - consslice.power_kwh

                charge = excess
                if charge > (batmax - bat):
                    charge = batmax - bat

                bat += charge
                excess -= charge

                self.batteryProductionConsumed.add_slice(consslice.clone())
                self.batteryGridExport.add_slice(prodslice.derive(excess))
                self.batteryGridImport.add_slice(prodslice.derive(0))
                self.batteryGridFlow.add_slice(prodslice.derive(excess))
                self.batteryCharged.add_slice(prodslice.derive(charge))
                self.batteryDischarged.add_slice(prodslice.derive(0))
                self.batteryFlow.add_slice(prodslice.derive(charge))
                self.batteryLevel.add_slice(prodslice.derive(bat))

        return self

    def summarize(self):
        for array in self.productionArrays:
            print(array.text())

        print(self.productionTotal.text())

        print(self.consumption.text())

        print(self.productionRelative.text())
        print(
            "Production (Excess): %d kwh"
            % (self.productionTotal.power() - self.consumption.power())
        )
        print(self.productionConsumed.text())

        print(self.gridImport.text())
        print(self.gridExport.text())
        print(self.gridFlow.text())

        if self.batteryProductionConsumed is None:
            return

        print(self.batteryProductionConsumed.text())

        print(self.batteryGridImport.text())
        print(self.batteryGridExport.text())
        print(self.batteryGridFlow.text())
        print(self.batteryCharged.text())

    def summarize_csv(self, headers=True):
        records = []
        records.extend(self.productionArrays)
        records.append(self.productionTotal)
        records.append(self.consumption)
        records.append(self.productionRelative)
        records.append(self.productionConsumed)
        records.append(self.gridImport)
        records.append(self.gridExport)
        records.append(self.gridFlow)
        if self.batteryProductionConsumed is not None:
            records.append(self.batteryProductionConsumed)
            records.append(self.batteryGridImport)
            records.append(self.batteryGridExport)
            records.append(self.batteryGridFlow)
            records.append(self.batteryCharged)

        labels = ["Report"] + [r.label for r in records]
        values = [self.label] + ["%d" % r.power() for r in records]

        if headers:
            print(",".join(labels))
        print(",".join(values))

    def save_csv(self):
        for array in self.productionArrays:
            array.save_csv()
        self.consumption.save_csv()

        self.productionTotal.save_csv()
        self.productionRelative.save_csv()
        self.productionConsumed.save_csv()
        self.gridImport.save_csv()
        self.gridExport.save_csv()
        self.gridFlow.save_csv()

        if self.batteryProductionConsumed is None:
            return

        self.batteryProductionConsumed.save_csv()
        self.batteryGridImport.save_csv()
        self.batteryGridExport.save_csv()
        self.batteryGridFlow.save_csv()

        self.batteryCharged.save_csv()
        self.batteryDischarged.save_csv()
        self.batteryFlow.save_csv()
        self.batteryLevel.save_csv()

    def visualize(self):
        for array in self.productionArrays:
            array.visualize()
        self.consumption.visualize()

        self.productionTotal.visualize()
        self.productionRelative.visualize()
        self.productionConsumed.visualize()
        self.gridImport.visualize()
        self.gridExport.visualize()
        self.gridFlow.visualize()

        if self.batteryProductionConsumed is None:
            return

        self.batteryProductionConsumed.visualize()
        self.batteryGridImport.visualize()
        self.batteryGridExport.visualize()
        self.batteryGridFlow.visualize()

        self.batteryCharged.visualize()
        self.batteryDischarged.visualize()
        self.batteryFlow.visualize()
        self.batteryLevel.visualize()

    def render(self):
        rows = []
        rows.append([self.consumption])
        rows.append(self.productionArrays)
        rows.append([self.productionTotal, self.productionRelative])
        rows.append([self.productionConsumed, self.batteryProductionConsumed])
        rows.append([self.gridImport, self.batteryGridImport])
        rows.append([self.gridExport, self.batteryGridExport])
        rows.append([self.gridFlow, self.batteryGridFlow])
        rows.append([self.batteryCharged, self.batteryDischarged])
        rows.append([self.batteryFlow, self.batteryLevel])

        with open("report.html", "w") as fh:
            print("<html><body>\n", file=fh)
            plotlyjs = True
            for records in rows:
                print('<div style="display: grid; grid-template-columns: 1fr 1fr">', file=fh)
                for record in records:
                    print('<div>', file=fh)
                    print(record.to_html(plotlyjs), file=fh)
                    print(
                        '<div style="font-size: smaller; margin-left: 6em; padding-bottom: 2em">%s</div>'
                        % record.description,
                        file=fh,
                    )
                    print('</div>', file=fh)
                    plotlyjs = False
                print('</div>', file=fh)
            print("</body></html>\n", file=fh)

    def charts(self):
        charts = [self.consumption] + self.productionArrays + \
            [self.productionTotal, self.productionRelative,
             self.productionConsumed, self.batteryProductionConsumed,
             self.gridImport, self.batteryGridImport,
             self.gridExport, self.batteryGridExport,
             self.gridFlow, self.batteryGridFlow,
             self.batteryCharged, self.batteryDischarged,
             self.batteryFlow, self.batteryLevel]

        for chart in charts:
            with open("report-%s.html" % chart.key, "w") as fh:
                print("<html><body>\n", file=fh)
                print('<div>', file=fh)
                print(chart.to_html(), file=fh)
                print(
                    '<div style="font-size: smaller; margin-left: 6em; padding-bottom: 2em">%s</div>'
                    % chart.description,
                    file=fh,
                )
                print('</div>', file=fh)
                print('</div>', file=fh)
                print("</body></html>\n", file=fh)
