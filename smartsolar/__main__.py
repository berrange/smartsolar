#!/usr/bin/python
# SPDX-License-Identifier: GPL-2.0-or-later

import yaml
import sys
import argparse

from .report import *

def parser():
    parser = argparse.ArgumentParser("Smart Solar")
    parser.add_argument(
        "--config",
        "-c",
        default="smartsolar.yaml",
        help="Path to configuration file (default=smartsolar.yaml)",
    )

    return parser


def summary():
    p = parser()
    p.add_argument("--csv",
                   action="store_true",
                   help="Report in CSV format")
    args = p.parse_args()

    report = Report.load_file(args.config)
    if args.csv:
        report.summarize_csv()
    else:
        report.summarize()


def dump():
    p = parser()
    args = p.parse_args()

    report = Report.load_file(args.config)
    report.save_csv()


def display():
    p = parser()
    args = p.parse_args()

    report = Report.load_file(args.config)
    report.visualize()


def report():
    p = parser()
    p.add_argument("--split",
                   action="store_true",
                   help="Generate separte chart per file")
    args = p.parse_args()

    report = Report.load_file(args.config)

    if args.split:
        report.charts()
    else:
        report.render()


def scenarios():
    p = parser()
    p.add_argument("--array",
                   action="append", default=[],
                   help="Array panel count variations: min:step:max")
    p.add_argument("--battery",
                   action="append", default=[],
                   help="Battery variations (kwh): min:step:max")
    args = p.parse_args()

    reports = Report.load_scenarios(args.config, args.array, args.battery)
    first = True
    for report in reports:
        report.summarize_csv(headers=first)
        first = False
