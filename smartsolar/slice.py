#!/usr/bin/python
# SPDX-License-Identifier: GPL-2.0-or-later

from datetime import date


class PowerSlice:
    def __init__(self, year, month, day, hour, minute, power_kwh):
        self.year = year
        self.month = month
        self.day = day
        self.hour = hour
        self.minute = minute

        self.power_kwh = power_kwh

    def key(self):
        # Skip "minute" since slices cover a full hour
        return f"{self.date():03}-{self.hour:02}"

    def date(self):
        return date(self.year, self.month, self.day).timetuple().tm_yday

    def __repr__(self):
        return f"date={self.date}, time={self.time} power={self.power_kwh}"

    def format_csv(self, fh):
        print(f"{self.date()},{self.hour},{self.power_kwh}", file=fh)

    def derive(self, power_kwh):
        return PowerSlice(
            self.year, self.month, self.day, self.hour, self.minute, power_kwh
        )

    def delta(self, power_kwh):
        return self.derive(self.power_kwh + power_kwh)

    def clone(self):
        return self.delta(0)

    @staticmethod
    def combine(slices):
        ref = slices[0]
        power_kwh = 0
        for s in slices:
            power_kwh += s.power_kwh
        return ref.derive(power_kwh)


class SolarPowerSlice(PowerSlice):
    def __init__(
        self,
        year,
        month,
        day,
        hour,
        minute,
        power_kwh,
        irradiance_w_m2,
        sun_height_deg,
        air_temp_c,
        wind_m_s,
    ):
        super().__init__(year, month, day, hour, minute, power_kwh)

        self.irradiance_w_m2 = irradiance_w_m2
        self.sun_height_deg = sun_height_deg
        self.air_temp_c = air_temp_c
        self.wind_m_s = wind_m_s
