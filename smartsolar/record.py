#!/usr/bin/python
# SPDX-License-Identifier: GPL-2.0-or-later

from clint.textui import progress
from datetime import datetime, timedelta
import json
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import requests
import os
import hashlib

from .slice import *


class PowerRecord:
    def __init__(self, key, label, description):
        self.key = key
        self.label = label
        self.description = description
        self.slices = {}

    def add_slice(self, power):
        self.slices[power.key()] = power

    def format_csv(self, fh):
        print("Date,Hour,Power(w)", file=fh)

        then = None
        for key in sorted(self.slices.keys()):
            slice = self.slices[key]
            now = slice.date()
            if then is not None and then != now:
                print("", file=fh)
            then = now
            slice.format_csv(fh)

    def save_csv(self):
        filename = self.key + ".csv"
        with open(filename, "w", encoding="utf8") as fh:
            self.format_csv(fh)

    def find(self, key):
        return self.slices.get(key, None)

    def text(self):
        return f"%s: %d kwh" % (self.label, self.power())

    def power(self):
        power = 0
        for slice in self.slices.values():
            power += slice.power_kwh
        return power

    def surface(self):
        points = []
        for i in range(365):
            points.append(
                [
                    0.0,
                    0.0,
                    0.0,
                    0.0,
                    0.0,
                    0.0,
                    0.0,
                    0.0,
                    0.0,
                    0.0,
                    0.0,
                    0.0,
                    0.0,
                    0.0,
                    0.0,
                    0.0,
                    0.0,
                    0.0,
                    0.0,
                    0.0,
                    0.0,
                    0.0,
                    0.0,
                    0.0,
                ]
            )

        for key in sorted(self.slices.keys()):
            slice = self.slices[key]
            points[slice.date() - 1][slice.hour] = slice.power_kwh

        return go.Surface(z=points)

    def figure(self):
        fig = go.Figure()
        fig.add_trace(self.surface())
        fig.update_traces(
            contours_z=dict(
                show=True, usecolormap=True, highlightcolor="limegreen", project_z=True
            )
        )

        camera = dict(
            up=dict(x=0, y=0, z=1),
            center=dict(x=0, y=0, z=0),
            eye=dict(x=-0.75, y=-2, z=1.25),
        )
        fig.update_layout(
            title=self.label,
            autosize=False,
            width=700,
            height=700,
            scene_camera=camera,
            scene=dict(
                xaxis_title="hour of day",
                yaxis_title="day of year",
                zaxis_title="energy kWh",
            ),
            margin=dict(l=65, r=50, b=65, t=90),
        )

        return fig

    def visualize(self):
        fig = self.figure()
        fig.show()

    def to_html(self, plotlyjs=True):
        fig = self.figure()
        return fig.to_html(full_html=False, include_plotlyjs=plotlyjs)


class Location:
    def __init__(self, longitude, latitude):
        self.longitude = longitude
        self.latitude = latitude


class Database:
    def __init__(self, name="PVGIS-SARAH2", start="2020", end="2020"):
        self.name = name
        self.start = start
        self.end = end


class SolarArray(PowerRecord):

    TOOL_HOURLY_DATA = "seriescalc"

    def __init__(self, label, elevation, azimuth, panels, capacity):
        super().__init__(
            "production-array-" + label,
            "Production (Array " + label + ")",
            "Energy generated from one single defined solar array",
        )
        self.elevation = elevation
        self.azimuth = azimuth
        self.panels = panels
        self.capacity = capacity
        self.name = label

    @staticmethod
    def tool_url(toolname, params):
        return (
            "https://re.jrc.ec.europa.eu/api/v5_2/"
            + toolname
            + "?"
            + "&".join([f"{k}={params[k]}" for k in params.keys()])
        )


    def cache_file(self, url):
        return "cache-pvgis-" + hashlib.sha256(url.encode("utf8")).hexdigest() + ".json"

    def read_cache(self, url):
        filename = self.cache_file(url)
        if not os.path.exists(filename):
            return None
        with open(filename, "rb") as fh:
            return fh.read()

    def save_cache(self, url, data):
        filename = self.cache_file(url)
        with open(filename, "wb") as fh:
            fh.write(data)

    def fetch_url(self, url):
        res = requests.get(url, stream=True)

        reslen = int(res.headers.get("content-length"))
        data = bytes([])
        for chunk in progress.bar(
            res.iter_content(chunk_size=10240),
            expected_size=(reslen / 10240) + 1,
            label=f"Fetch PV GIS {self.name}: ",
        ):
            if chunk:
                data += chunk

        return data

    def fetch_data(self, url):
        data = self.read_cache(url)
        if data is None:
            data = self.fetch_url(url)
            self.save_cache(url, data)
        return data

    def fetch_pvgis(self, location, database):
        params = {
            "lon": location.longitude,
            "lat": location.latitude,
            "raddatabase": database.name,
            "browser": "0",
            "outputformat": "json",
            "userhorizon": "",
            "usehorizon": "0",
            "angle": self.elevation,
            "aspect": self.azimuth,
            "startyear": database.start,
            "endyear": database.end,
            "pvcalculation": "1",
            "peakpower": ((self.panels * self.capacity) / 1000.0),
            "mountplace": "building",
            "loss": "14",
            "select_database_hourly": database.name,
        }

        url = self.tool_url(self.TOOL_HOURLY_DATA, params)
        data = self.fetch_data(url)
        res = json.loads(data)

        for rec in res["outputs"]["hourly"]:
            ts = rec["time"]

            year = int(ts[0:4])
            month = int(ts[4:6])
            day = int(ts[6:8])
            hour = int(ts[9:11])
            minute = int(ts[11:13])

            self.add_slice(
                SolarPowerSlice(
                    year=year,
                    month=month,
                    day=day,
                    hour=hour,
                    minute=minute,
                    power_kwh=(rec["P"] / 1000.0),
                    irradiance_w_m2=rec["G(i)"],
                    sun_height_deg=rec["H_sun"],
                    air_temp_c=rec["T2m"],
                    wind_m_s=rec["WS10m"],
                )
            )


class Consumption(PowerRecord):
    def __init__(self):
        super().__init__(
            "consumption-no-solar",
            "Consumption (no solar)",
            "Energy consumed by the property reported by smart meter prior to deploying solar PV"
        )

    def load_octopus(self, datafile):
        with open(datafile, "r", encoding="utf8") as fh:
            skip = True
            last = None
            for line in fh:
                if skip:
                    skip = False
                    continue
                fields = line.split(",")
                ts = fields[1].strip()

                year = int(ts[0:4])
                month = int(ts[5:7])
                day = int(ts[8:10])

                hour = int(ts[11:13])
                minute = int(ts[14:16])

                then = datetime(year, month, day, hour, minute) - timedelta(minutes=30)

                power_kwh = float(fields[0])

                # Merging together all records from within the same
                # 1 hour window
                if last is None or last.hour != then.hour:
                    last = PowerSlice(
                        year=then.year,
                        month=then.month,
                        day=then.day,
                        hour=then.hour,
                        minute=then.minute,
                        power_kwh=power_kwh,
                    )
                    self.add_slice(last)
                else:
                    last.power_kwh += power_kwh
        
    def load_n3rgy(self, datafile):
        with open(datafile, "r", encoding="utf8") as fh:
            skip = True
            last = None
            for line in fh:
                if skip:
                    skip = False
                    continue
                fields = line.split(",")
                ts = fields[0]

                year = int(ts[1:5])
                month = int(ts[6:8])
                day = int(ts[9:11])

                hour = int(ts[12:14])
                minute = int(ts[15:17])

                then = datetime(year, month, day, hour, minute) - timedelta(minutes=30)

                power_kwh = float(fields[1][1:-2])

                # Merging together all records from within the same
                # 1 hour window
                if last is None or last.hour != then.hour:
                    last = PowerSlice(
                        year=then.year,
                        month=then.month,
                        day=then.day,
                        hour=then.hour,
                        minute=then.minute,
                        power_kwh=power_kwh,
                    )
                    self.add_slice(last)
                else:
                    last.power_kwh += power_kwh


class ProductionTotal(PowerRecord):
    def __init__(self, productionArrays):
        super().__init__(
            "production-total",
            "Production: Total",
            "Combined energy generated by all defined solar arrays",
        )

        for k in productionArrays[0].slices.keys():
            self.add_slice(
                PowerSlice.combine([rec.slices[k] for rec in productionArrays])
            )


class ProductionRelative(PowerRecord):
    def __init__(self, productionTotal, consumption):
        super().__init__(
            "production-relative",
            "Production: Relative",
            "Combined energy generated by all defined solar arrays, for time slices where consumption information is available",
        )

        for consslice in consumption.slices.values():
            prodslice = productionTotal.find(consslice.key())
            if prodslice is None:
                prodslice = consslice.derive(0)
            self.add_slice(prodslice)


class ProductionConsumed(PowerRecord):
    def __init__(self, productionRelative, consumption):
        super().__init__(
            "production-consumed-no-battery",
            "Production: Consumed (no battery)",
            "Generated energy that could be consumed locally on-site",
        )

        for consslice in consumption.slices.values():
            prodslice = productionRelative.find(consslice.key())
            consumed = consslice.power_kwh
            if consumed > prodslice.power_kwh:
                consumed = prodslice.power_kwh
            self.add_slice(consslice.derive(consumed))


class GridImport(PowerRecord):
    def __init__(self, productionRelative, consumption):
        super().__init__(
            "grid-import-no-battery",
            "Grid: Import (no battery)",
            "Energy imported from the grid due to insufficient local generation to satisfy consumption",
        )

        for consslice in consumption.slices.values():
            prodslice = productionRelative.find(consslice.key())
            imported = 0
            if consslice.power_kwh > prodslice.power_kwh:
                imported = consslice.power_kwh - prodslice.power_kwh
            self.add_slice(consslice.derive(imported))


class GridExport(PowerRecord):
    def __init__(self, productionRelative, consumption):
        super().__init__( 
            "grid-export-no-battery",
            "Grid: Export (no battery)",
            "Energy exported to the grid due to production from solar arrays exceeding consumption",
        )

        for consslice in consumption.slices.values():
            prodslice = productionRelative.find(consslice.key())
            exported = 0
            if prodslice.power_kwh > consslice.power_kwh:
                exported = prodslice.power_kwh - consslice.power_kwh
            self.add_slice(consslice.derive(exported))


class GridFlow(PowerRecord):
    def __init__(self, gridImport, gridExport):
        super().__init__(
            "grid-flow-no-battery",
            "Grid: Flow (no battery)",
            "Energy imported (-ve) and exported (+ve) from/to the grid due to mismatched production and consumption",
        )

        for impslice in gridImport.slices.values():
            expslice = gridExport.find(impslice.key())
            imex = expslice.power_kwh
            if imex == 0:
                imex = impslice.power_kwh * -1
            self.add_slice(impslice.derive(imex))


class BatteryProductionConsumed(PowerRecord):
    def __init__(self):
        super().__init__(
            "production-consumed-with-battery",
            "Production: Consumed (with battery)",
            "Generated energy that could be consumed locally, or stored in the battery",
        )


class BatteryGridImport(PowerRecord):
    def __init__(self):
        super().__init__(
            "grid-import-with-battery",
            "Grid: Import (with battery)",
            "Energy imported from the grid due to insufficient generation and empty battery",
        )


class BatteryGridExport(PowerRecord):
    def __init__(self):
        super().__init__(
            "grid-export-with-battery",
            "Grid: Export (with battery)",
            "Energy exported to the grid due to generation exceeding ocnsumption and available unused battery capacity",
        )


class BatteryGridFlow(PowerRecord):
    def __init__(self):
        super().__init__(
            "grid-flow-with-battery",
            "Grid: Flow (with battery)",
            "Energy imported (-ve) and exported (+ve) from/to the grid due to mismatched consumption vs production and battery capacity",
        )


class BatteryStorageCharged(PowerRecord):
    def __init__(self):
        super().__init__(
            "storage-charged-with-battery",
            "Storage: Charged (with battery)",
            "Energy added to the battery due to production exceeding array generation",
        )


class BatteryStorageDischarged(PowerRecord):
    def __init__(self):
        super().__init__(
            "storage-discharged-with-battery",
            "Storage: Discharged (with battery)",
            "Energy discharged from the battery due to insufficient generation to satisfy consumption",
        )


class BatteryStorageFlow(PowerRecord):
    def __init__(self):
        super().__init__(
            "storage-flow-with-battery",
            "Storage: Flow (with battery)",
            "Energy added (+ve) and discharged (-ve) to/from the battery due to generation excess or shortfall vs consumption",
        )


class BatteryStorageLevel(PowerRecord):
    def __init__(self):
        super().__init__(
            "storage-level-with-battery",
            "Storage: Level (with battery)",
            "Energy stored in the battery based on consumption and generation prior to each time slice",
        )
